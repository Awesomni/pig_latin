"""
Модуль для поросячего латыня.

Можно выполнить напрямую и тогда он будет вечно спрашивать текст для кодирования,
а можно импортировать и вызвать функцию pig_latin.
"""


def pig_latin_word(word: str) -> str:
    """Получает слово для кодирования его в поросячий латынь."""
    excluded_words = ('a', 'the', 'an', 'on', 'to', 'at', 'in', 'into', 'as', )
    if word in excluded_words:
        return word

    suffix = ''
    if word[0].lower() in 'eyuioa':
        suffix = 'way'
    if word[0].lower() in 'qwrtpsdfghjklzxcvbnm':
        suffix = 'ay'

    return f'{word[1:]}{word[0]}{suffix}'


def pig_latin(phrase: str) -> str:
    """Переводит фразу на поросячий латынь."""
    words = phrase.split(' ')
    return ' '.join([pig_latin_word(word) for word in words])


def main():
    """Run main code."""
    while True:
        text = input('Введите фразу для перевода в поросячий латынь или нажмите Enter, чтобы выйти: ')
        if text == '':
            break
        print(pig_latin(text))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()
    print('Работа программы завершена')
